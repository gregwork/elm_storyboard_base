module Adapter.AuthEmail exposing
    ( Login
    , Redirect
    , encodeLogin
    , login
    , loginToString
    , redirect
    )

import Dict exposing (Dict, map, toList)
import Json.Decode as Jdec
import Json.Decode.Pipeline as Jpipe
import Json.Encode as Jenc
import List exposing (map)


type alias Login =
    { email : String
    }


type alias Redirect =
    { redirect : String
    }



-- decoders and encoders


loginToString : Login -> String
loginToString r =
    Jenc.encode 0 (encodeLogin r)


login : Jdec.Decoder Login
login =
    Jdec.succeed Login
        |> Jpipe.required "email" Jdec.string


encodeLogin : Login -> Jenc.Value
encodeLogin x =
    Jenc.object
        [ ( "email", Jenc.string x.email )
        ]


redirect : Jdec.Decoder Redirect
redirect =
    Jdec.succeed Redirect
        |> Jpipe.required "redirect" Jdec.string



--- encoder helpers
-- makeListEncoder : (a -> Jenc.Value) -> List a -> Jenc.Value
-- makeListEncoder f arr =
--     Jenc.list (List.map f arr)


makeDictEncoder : (a -> Jenc.Value) -> Dict String a -> Jenc.Value
makeDictEncoder f dict =
    Jenc.object (toList (Dict.map (\k -> f) dict))


makeNullableEncoder : (a -> Jenc.Value) -> Maybe a -> Jenc.Value
makeNullableEncoder f m =
    case m of
        Just x ->
            f x

        Nothing ->
            Jenc.null
