module Model exposing (ContentModel, Domain(..), HttpFrom(..), HttpModel, InputStr(..), Model, NavModel, StoryboardPortion(..), domainFor, newHttpModel, urlToDomain)

import Adapter.Content exposing (ContentData)
import Array exposing (Array)
import Browser.Navigation as Nav
import Dict exposing (Dict)
import Html exposing (Html)
import Json.Decode
import ModelBase
import Route
import Set exposing (Set)
import Url


type alias Model =
    { contentModelResult : Result String ContentModel
    , httpModel : HttpModel
    , nav : NavModel
    , storyboardPortion : StoryboardPortion
    , storyboardPortionIndex : Int
    , storyboardPortionArray : Array StoryboardPortion
    , version : Int
    }


type alias ContentModel =
    { content : ContentData
    }


type InputStr
    = InputStrNoOp


type Domain
    = UnknownDomain


type alias HttpModel =
    { httpResponseText : String
    , httpFrom : HttpFrom
    , initialDataLoaded : Bool
    }


type alias NavModel =
    { domain : Domain
    , domainOverride : Maybe Domain
    , key : Nav.Key
    , routeActive : Route.Route
    , url : Url.Url
    }


type HttpFrom
    = HttpFromNone


type StoryboardPortion
    = StoryboardPortion1
    | StoryboardPortion2
    | StoryboardPortion3
    | StoryboardPortion4
    | StoryboardPortion5


newHttpModel : HttpModel
newHttpModel =
    { httpResponseText = "", httpFrom = HttpFromNone, initialDataLoaded = True }


urlToDomain : String -> Domain
urlToDomain host =
    let
        domain =
            String.join "." <| List.reverse <| List.take 2 <| List.reverse <| String.split "." host
    in
    case domain of
        "localhost" ->
            UnknownDomain

        _ ->
            UnknownDomain


domainFor : Maybe Domain -> Url.Url -> ( Domain, Maybe Domain )
domainFor domainOverride url =
    let
        newDomainOverride =
            case domainOverride of
                Just override ->
                    domainOverride

                Nothing ->
                    if url.host == "localhost" || url.host == "something.ngrok.io" then
                        Maybe.map urlToDomain url.query

                    else
                        Nothing

        domain =
            let
                initial =
                    Maybe.withDefault (urlToDomain url.host) newDomainOverride
            in
            if initial == UnknownDomain then
                UnknownDomain

            else
                initial
    in
    ( domain, newDomainOverride )
