# Storyboard

## Startup

### Install

```
git clone <repo>
cd <repo>/files
npm install
npm run dev

# start postgres
./wsl.sh
ngrok http 1234 # port from package.json run dev
cd DOMAIN_NAME/DOMAIN_NAME_api/files && cargo watch -x check -x run # if using rust
```

### wsl.sh

```
#!/usr/bin/env bash
# code for: wsl.sh

# npm install -g local-web-server
# https://www.npmjs.com/package/local-web-server

# exec ws --rewrite '/data_api/* -> http://localhost:4501/data_api/$1' --rewrite '/* -> http://localhost:5000/$1'
# exec ws -p 8010 --rewrite '/api/* -> http://localhost:8000/api/$1' --rewrite '/* -> http://localhost:8002/$1'

exec ws -p 1234 --rewrite '/graphql -> http://localhost:5000/graphql' --rewrite '/auth/* -> http://localhost:5000/auth/$1' --rewrite '/oauth/* -> http://localhost:5000/oauth/$1' --rewrite '/* -> http://localhost:1235/$1'
```
